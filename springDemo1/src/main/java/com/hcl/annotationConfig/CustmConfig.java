package com.hcl.annotationConfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.hcl.annotationBased")
public class CustmConfig 
{

}
